import React from "react";
import Cart from "../Cart/Cart";
import "./header.scss";
import Logo2 from "../../img/star2.svg";

export default class Header extends React.Component {

    render() {
        return (
            <>
                <div className="header-container">
                <p className="current-number">{this.props.count}</p>
                <img alt="favorite" src= {Logo2} num={this.props.count}></img>
                <Cart num={this.props.cartCount} />
                </div>
            </>
        )

    }
}