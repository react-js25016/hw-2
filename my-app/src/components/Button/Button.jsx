import React from "react";
import "./button.scss";

export default class Button extends React.Component{

    static defaultProps = {
        color: "yellow",
        code: "3",
        text: "Click me"
    }
    
    render(){
        return (
            <button id={this.props.id} data-modal-id={this.props.code} onClick={ (e) => this.props.onClick(e) } className="button" style={{backgroundColor: this.props.color}}>{this.props.text}</button>
        )
    }
}