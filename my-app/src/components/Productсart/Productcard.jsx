import React from "react";
import "./productcard.scss";
import Logo1 from "../../img/star1.svg";
import Logo2 from "../../img/star2.svg";

export default class ProductCard extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            fill: false,
        }
    }

    addFavorits = () => {
        this.state.fill === true ? this.setState({ fill: false }) : this.setState({ fill: true});
       
    }

    componentDidMount = () => {
       let dataLS = localStorage.getItem("favorId");
       let arr = JSON.parse(dataLS);
       let currentItem = arr.find(item => item === this.props.id);
       currentItem ? this.setState(({fill: true})) : this.setState({fill:false});
    }

    render(){
        return(
            <>
                <div id={this.props.id} className="product-card">
                    <img alt="photo_flower" className="product-card-img" src={this.props.src}  width={350} height={230}></img>
                    <div className="product-card-content">
                    <h4 className="product-card-name">Name: {this.props.name}</h4>
                    <p className="product-card-price">Price: {this.props.price} ₴</p>
                    <p className="product-card-code">Code: {this.props.code}</p>
                    <p className="product-card-color">Color: {this.props.color}</p>
                    <img  className = {this.state.fill === true ? "removed-from-favorite" : "add-to-favorite"} id={this.props.id} alt="logo" src={this.state.fill === true ? Logo1 : Logo2 } onClick={this.addFavorits}></img>
                    </div>
                </div>
            </>
        )
    }
}