import React from "react";
import Button from "../Button/Button";
import ProductCard from "../Productсart/Productcard";
import "./cardbox.scss";



export default class ProductList extends React.Component {
    render() {
        return (
            <>
                <div className="card-box" onClick={this.props.click}>
                {this.props.products.map((item) => {
                        return (
                            <div key={item.code} className="card">
                                <ProductCard id={item.code} src={item.url} name={item.name} price={item.price} code={item.code} color={item.color} {...item} />
                                <div className="button-wrapper">
                                <Button code="3" onClick={this.props.show} color="pink" text="Add to cart" id = {item.code}/>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </>
        );
    }
}