export const modalWindowDeclarations = [
    {
        id: '1',
        header: "Do you want to delete this file?",
        closeButton: true,
        text: 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?',
        actions: ["Ok", "Cancel"],
      },

      {
        id: '2',
        header: "Добавить товар в корзину?",
        closeButton: false,
        text: 'Подтверждение добавления товара в корзину',
        actions: ["Сохранить", "Отменить"],
      },

      {
        id: '3',
        header: "Add product to cart?",
        closeButton: true,
        text: 'Confirmation of adding a product to the cart',
        actions: ["Save", "Close"],
      },
]