import React from "react";
import "./modal.scss";

export default class Modal extends React.Component {

    static defaultProps = {
        header: "Confirmation",
        closeButton: true,
        text: "Click 'Yes' or 'No'",
        actions: ["Yes", "No"],
    }

    render() {
        return (
            <>
                <div className={this.props.className} onClick={this.props.close}>
                    <div className="modal" onClick={(e) => e.stopPropagation()}>
                        <div className="modal-header">
                            <h4 className="title">{this.props.header}</h4>
                            <span onClick={this.props.close} className="header-close">{this.props.closeButton ? "╳" : ""}</span>
                        </div>
                        <div className="modal-content">
                            <div className="modal-body">
                                <p className="text">{this.props.text}</p>
                            </div>
                            <div className="modal-footer">
                                <button className="btn" onClick={this.props.save}>{this.props.actions[0]}</button>
                                <button className="btn" onClick={this.props.close}>{this.props.actions[1]}</button>
                            </div></div>
                    </div>
                </div>
            </>
        );
    }
}

