import React from "react";
import Header from "../../components/Header/Header";
import ProductList from "../../components/ProductList/ProductList";
import { modalWindowDeclarations } from "../../components/Modal/modalWindow";
import Modal from "../../components/Modal/Modal";

export default class Home extends React.Component {
    state = {

        count: localStorage.getItem("count") ? (Number(localStorage.getItem("count"))) : 0,
        cartCount: localStorage.getItem("cartCount") ? (Number(localStorage.getItem("cartCount"))) : 0,
        open: false,
        content: {
            header: "",
            text: "",
            actions: [],
            closeButton: true,
        },
        products: [],
        currentproduct: [],
        favorId: [],
        cartId: [],
    }

    componentDidMount() {
        try {
            fetch("./flowers.json")
                .then(response => response.json())
                .then(data => this.setState({ products: [...data] }))
        } catch (e) {
            console.log(e.message);
        }

        let dataLS = localStorage.getItem("favorId");
        let arrId = JSON.parse(dataLS);
        arrId ? this.setState({favorId: arrId}) : this.setState({favorId: []});

        let dataCartLS = localStorage.getItem("cartId");
        let arrCartId = JSON.parse(dataCartLS);
        arrCartId ? this.setState({cartId: arrCartId}) : this.setState({cartId: []});
    }

    closeModal = (e) => {
        e.target.closest(".active") ? this.setState({ open: false }) : this.setState({ open: true, status: {} });
    }

    showModal = (e) => {
        let modalId = e.target.getAttribute("id");
        let currentproduct = this.state.products.filter((item) => item.code === modalId);
        this.setState({ currentproduct: currentproduct });
        let modalData = e.target.getAttribute("data-modal-id");
        let currentItem = modalWindowDeclarations.find(item => item.id === modalData);
        this.setState({ open: true, content: { ...currentItem } })
    }

    addFavorites = (e) => {
        if(e.target.tagName !== "IMG") return false;
        
        e.target.className === "add-to-favorite" ?
            this.setState((prev) => ({ count: ++prev.count, favorId: [...prev.favorId, e.target.id]}))
            : this.setState((prev) => ({ count: --prev.count, favorId: [...prev.favorId, e.target.id].filter(el => el !== e.target.id)}));
    }

    addCart = (e) => {
        this.setState((prev) => ({ cartCount: prev.cartCount + 1, cartId: [...prev.cartId, this.state.currentproduct]}));
        let product = this.state.currentproduct.map(item => item.name).join("");
        console.log(`В корзину будет добавлен ${product}`);
        e.target.closest(".active") ? this.setState({ open: false }) : this.setState({ open: true, status: {} });
    }

    componentDidUpdate() {
        
        localStorage.setItem("count", JSON.stringify(this.state.count));
        localStorage.setItem("cartCount", JSON.stringify(this.state.cartCount));
        localStorage.setItem("favorId", JSON.stringify(this.state.favorId));
        localStorage.setItem("cartId", JSON.stringify(this.state.cartId));
    }

    render() {
        return (
            <>
           
                <Header count={this.state.count} cartCount={this.state.cartCount} /> 
                <ProductList click={this.addFavorites} show={this.showModal} products={this.state.products} />
                <Modal
                    className={this.state.open ? "modal-wrapper active" : "modal-wrapper"}
                    save={this.addCart}
                    close={this.closeModal}
                    header={this.state.content.header}
                    text={this.state.content.text}
                    closeButton={this.state.content.closeButton}
                    actions={this.state.content.actions}
                />
            </>
        )
    }
}
